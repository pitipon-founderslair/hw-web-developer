# HW - Web developer

## TODO
* Create Corportate page from Server mockup
* Use /server to connect api (https://gitlab.com/pitipon-founderslair/hw-web-developer/-/blob/main/server/README.md)
* Use NextJS
* (Optional) Create Upload a new project

## Duration
* 5-7 days

## Example of corporate project page
![example](https://gitlab.com/pitipon-founderslair/HW-Design-UI/-/raw/main/example-corporate-projects.png)
